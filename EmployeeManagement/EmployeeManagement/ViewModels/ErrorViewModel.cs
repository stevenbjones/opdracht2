﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement.ViewModels
{
    public class ErrorViewModel
    {
        public string ExceptionPath { get; set; }

        public string ExceptionMessage { get; set; }
    
    }
}
