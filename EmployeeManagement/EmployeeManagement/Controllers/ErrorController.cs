﻿using EmployeeManagement.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EmployeeManagement.Controllers
{
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> _logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            _logger = logger;
        }

        [Route("Error/{statusCode}")]
        public IActionResult NotFound(int statusCode)
        {
            ErrorViewModel errorViewModel = new ErrorViewModel();
            var statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();

            switch (statusCode)
            {
                case 404:
                    errorViewModel.ExceptionMessage = "Sorry, the resource you requested could not be found";
                    errorViewModel.ExceptionPath = $"404 Error Occured. Path = {statusCodeResult.OriginalPath} ";
                    break;
            }
            return View(errorViewModel);
        }

        [AllowAnonymous]
        [Route("Error")]
        public IActionResult Error()
        {
            ErrorViewModel errorViewModel = new ErrorViewModel();
            // Retrieve the exception Details
            var exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            errorViewModel.ExceptionPath = $"The path {exceptionHandlerPathFeature.Path} threw an exception {exceptionHandlerPathFeature.Error} ";
            

            return View(errorViewModel);
        }
    }
}
